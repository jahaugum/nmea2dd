package main

import (
	"fmt"
	"os"
	"strconv"
	"strings"
)

func main() {
	coords := os.Args[1:]
	if len(coords) == 1 {
		coords = strings.Split(coords[0], ",")
	}

	if len(coords) == 4 {
		coords = []string{coords[0] + coords[1], coords[2] + coords[3]}
	}

	var dddd []string
	for _, coord := range coords {
		negate := false
		if strings.HasSuffix(coord, "W") || strings.HasSuffix(coord, "S") {
			negate = true
		}
		coord = strings.TrimRight(coord, "NSWE")
		gll, err := strconv.ParseFloat(coord, 64)
		if err != nil {
			panic(err)
		}
		degrees := int64(gll) / 100
		fraction := (gll - float64(degrees*100)) / 60
		utm := float64(degrees) + fraction
		if negate {
			utm = -utm
		}
		dddd = append(dddd, strconv.FormatFloat(utm, 'f', 6, 64))
	}
	fmt.Println("Deciaml Degrees Coordinates:", strings.Join(dddd, ","))
}
